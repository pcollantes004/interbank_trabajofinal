

public class Programa {
    public static void main(String[] args) {
        // Datos:
        String trabajador ="Jose";

        String[]TarjetadeCrédito={"Clásica","Gold","Platinum","TOP"};
        int []PuntosTCR={5,10,15,20};
        int []PuntosTCA={3,8,13,18};
        int CantVendidaClásica=0, Referido=0;
        int CantVendidaGold=0,    ReferidoG=6;
        int CantVendidaPlatinum=0,ReferidoP=0;
        int CantVendidaTop=0,     ReferidoT=0;

        String[]CréditosyPréstamos={"Digital","Efectivo","Express", "Estudios"};
        int []Puntos={0,0,0,0};
        int CantVendidaCréditosP=10;


        String[]CréditoVehicular={"Contado","Crédito36m","Crédito60m"};
        int []PuntosCVR={30,15,5};
        int []PuntosCVA={20,10,3};
        int CantVendidoCV=0,  ReferidoCV=0;
        int CantVendidoCV3=0, ReferidoCV3=0;
        int CantVendidoCV6=0, ReferidoCV6=0;


        int PuntosTCC = PuntosTarjetasdeCréditoClásica(TarjetadeCrédito,PuntosTCR,PuntosTCA,CantVendidaClásica, Referido);
        System.out.println("La cantidad total de puntos por Tarjeta Clásica: " + PuntosTCC);

        int PuntosTCG = PuntosTarjetasdeCréditoGold(TarjetadeCrédito,PuntosTCR,PuntosTCA,CantVendidaGold, ReferidoG);
        System.out.println("La cantidad total de puntos por Tarjeta Gold: " +PuntosTCG);

        int PuntosTCP = PuntosTarjetasdeCréditoPlatinum(TarjetadeCrédito,PuntosTCR,PuntosTCA,CantVendidaPlatinum,ReferidoP);
        System.out.println("La cantidad total de puntos por Tarjeta Platinum: " + PuntosTCP);

        int PuntosTCT = PuntosTarjetasdeCréditoTOP(TarjetadeCrédito,PuntosTCR,PuntosTCA,CantVendidaTop, ReferidoT);
        System.out.println("La cantidad total de puntos por Tarjeta TOP: " +PuntosTCT);

        int PuntosCP= CréditosyPréstamos(CréditosyPréstamos, Puntos, CantVendidaCréditosP);
        System.out.println("La cantidad total de puntos por Créditos y Préstamos: " + PuntosCP);

        int PuntosCV = PuntosCréditoVehicular(CréditoVehicular,PuntosCVR,PuntosCVA,CantVendidoCV, ReferidoCV);
        System.out.println("La cantidad total de puntos por Créditos Vehiculares al Contado: " + PuntosCV);

        int Puntos36m=PuntosCréditoVehicular36(CréditoVehicular,PuntosCVR,PuntosCVA,CantVendidoCV3,ReferidoCV3);
        System.out.println("La cantidad total de puntos por Créditos Vehiculares por 36 meses: " + Puntos36m);

        int Puntos60m=PuntosCréditoVehicular60(CréditoVehicular,PuntosCVR,PuntosCVA,CantVendidoCV6,ReferidoCV6);
        System.out.println("La cantidad total de puntos por Créditos Vehiculares por 60 meses: " + Puntos60m);

        int Puntostotales=Puntostotal(PuntosTCC,PuntosTCG,PuntosTCP,PuntosTCT,PuntosCP,PuntosCV,Puntos36m,Puntos60m);
        System.out.println("La cantidad total de puntos para el trabajador "+ trabajador + " es: " + Puntostotales);

        int CantidadTotalPF =CantidadTotal(CantVendidaClásica,Referido,CantVendidaGold,ReferidoG,CantVendidaPlatinum,ReferidoP,CantVendidaTop,ReferidoT,CantVendidaCréditosP, CantVendidoCV,ReferidoCV,CantVendidoCV3,ReferidoCV3,CantVendidoCV6,ReferidoCV6);
        System.out.println("La cantidad total de Productos financieros vendidos es:  "+ CantidadTotalPF);

        int Comisiontotal=ComisiónTotal(Puntostotales);
        System.out.println("El trabajador " + trabajador + " recibe un total de comisión de: S/ " + Comisiontotal + " soles");
    }
    // Metodo para calcular los Puntos totales  Tarjetas Clásicas
    private  static  int PuntosTarjetasdeCréditoClásica(String[] tarjetadeCrédito, int[] puntosTCR, int[] puntosTCA, int CantVendidaClásica, int Referido){
      int PuntosReferidos=0;
      int Puntostotales =0;
      int limite =5;
      int PuntosParciales=0;
      int TotalCantidad= Referido+CantVendidaClásica;
      for (int i=0; i< tarjetadeCrédito.length;i++){
          if ("Clásica".equals(tarjetadeCrédito[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
              Puntostotales = puntosTCR[i] * (Referido+ CantVendidaClásica);
          } else if ("Clásica".equals(tarjetadeCrédito[i]) && TotalCantidad> limite){
              PuntosParciales=puntosTCR[i] * limite;
              Puntostotales= PuntosParciales + (((Referido-limite) + CantVendidaClásica)*puntosTCA[i]);
          }
      }
      return  Puntostotales;
    }
    // Metodo para calcular los Puntos totales  Tarjetas Gold
    private  static  int PuntosTarjetasdeCréditoGold(String[] tarjetadeCrédito, int[] puntosTCR, int[] puntosTCA, int CantVendidaGold,int ReferidoG){
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite =5;
        int PuntosParciales=0;
        int TotalCantidad= ReferidoG+CantVendidaGold;
        for (int i=0; i< tarjetadeCrédito.length;i++){
            if ("Gold".equals(tarjetadeCrédito[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
                Puntostotales = puntosTCR[i] * (ReferidoG+ CantVendidaGold);
            } else if ("Gold".equals(tarjetadeCrédito[i]) && TotalCantidad> limite){
                PuntosParciales=puntosTCR[i] * limite;
                Puntostotales= PuntosParciales + (((ReferidoG-limite) + CantVendidaGold)*puntosTCA[i]);
            }
        }
        return  Puntostotales;
}
    // Metodo para calcular los Puntos totales  Tarjetas Platinum
    private  static  int PuntosTarjetasdeCréditoPlatinum(String[] tarjetadeCrédito, int[] puntosTCR, int[] puntosTCA, int CantVendidaPlatinum, int ReferidoP){
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite =5;
        int PuntosParciales=0;
        int TotalCantidad= ReferidoP+CantVendidaPlatinum;
        for (int i=0; i< tarjetadeCrédito.length;i++){
            if ("Platinum".equals(tarjetadeCrédito[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
                Puntostotales = puntosTCR[i] * (ReferidoP+ CantVendidaPlatinum);
            } else if ("Platinum".equals(tarjetadeCrédito[i]) && TotalCantidad> limite){
                PuntosParciales=puntosTCR[i] * limite;
                Puntostotales= PuntosParciales + (((ReferidoP-limite) + CantVendidaPlatinum)*puntosTCA[i]);
            }
        }
        return  Puntostotales;
    }
    // Metodo para calcular los Puntos totales  Tarjetas TOP
    private  static  int PuntosTarjetasdeCréditoTOP(String[] tarjetadeCrédito, int[] puntosTCR, int[] puntosTCA, int CantVendidaTOP, int ReferidoT){
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite =5;
        int PuntosParciales=0;
        int TotalCantidad= ReferidoT+CantVendidaTOP;
        for (int i=0; i< tarjetadeCrédito.length;i++){
            if ("TOP".equals(tarjetadeCrédito[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
                Puntostotales = puntosTCR[i] * (ReferidoT+ CantVendidaTOP);
            } else if ("TOP".equals(tarjetadeCrédito[i]) && TotalCantidad> limite){
                PuntosParciales=puntosTCR[i] * limite;
                Puntostotales= PuntosParciales + (((ReferidoT-limite) + CantVendidaTOP)*puntosTCA[i]);
            }
        }
        return  Puntostotales;
    }
    // Metodo para calcular los Puntos totales  Créditos y Préstamos
    private  static  int CréditosyPréstamos(String[] CréditosyPréstamos, int[] puntos, int CantVendidaCreditosP){
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite = 0;
        for (int i=0; i< CréditosyPréstamos.length;i++){
            if (CantVendidaCreditosP != 0) {
                Puntostotales =puntos[i];
            }
        }
        return  Puntostotales;
    }
    //Metodo para calcular los Puntos totales Créditos Vehicular Contado
    private static int PuntosCréditoVehicular(String[] créditoVehicular, int[] puntosCVR, int[] puntosCVA, int cantVendidoCV, int referidoCV) {
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite =3;
        int PuntosParciales=0;
        int TotalCantidad= referidoCV+cantVendidoCV;
        for (int i=0; i< créditoVehicular.length;i++){
            if ("Contado".equals(créditoVehicular[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
                Puntostotales = puntosCVR[i] * (referidoCV+ cantVendidoCV);
            } else if ("Contado".equals(créditoVehicular[i]) && TotalCantidad> limite){
                PuntosParciales=puntosCVR[i] * limite;
                Puntostotales= PuntosParciales + (((referidoCV-limite) + cantVendidoCV)*puntosCVA[i]);
            }
        }
        return  Puntostotales;
    }
    //Metodo para calcular los Puntos totales Créditos Vehicular 36 meses
    private static int PuntosCréditoVehicular36(String[] créditoVehicular, int[] puntosCVR, int[] puntosCVA, int cantVendidoCV3, int referidoCV3) {
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite =5;
        int PuntosParciales=0;
        int TotalCantidad= referidoCV3+cantVendidoCV3;
        for (int i=0; i< créditoVehicular.length;i++){
            if ("Crédito36m".equals(créditoVehicular[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
                Puntostotales = puntosCVR[i] * (referidoCV3+ cantVendidoCV3);
            } else if ("Crédito36m".equals(créditoVehicular[i]) && TotalCantidad> limite){
                PuntosParciales=puntosCVR[i] * limite;
                Puntostotales= PuntosParciales + (((referidoCV3-limite) + cantVendidoCV3)*puntosCVA[i]);
            }
        }
        return  Puntostotales;
    }
    //Metodo para calcular los Puntos totales Créditos Vehicular 60 meses
    private static int PuntosCréditoVehicular60(String[] créditoVehicular, int[] puntosCVR, int[] puntosCVA, int cantVendidoCV6, int referidoCV6) {
        int PuntosReferidos=0;
        int Puntostotales =0;
        int limite =2;
        int PuntosParciales=0;
        int TotalCantidad= referidoCV6+cantVendidoCV6;
        for (int i=0; i< créditoVehicular.length;i++){
            if ("Crédito60m".equals(créditoVehicular[i]) && TotalCantidad<=limite && TotalCantidad != 0) {
                Puntostotales = puntosCVR[i] * (referidoCV6+ cantVendidoCV6);
            } else if ("Crédito60m".equals(créditoVehicular[i]) && TotalCantidad> limite){
                PuntosParciales=puntosCVR[i] * limite;
                Puntostotales= PuntosParciales + (((referidoCV6-limite) + cantVendidoCV6)*puntosCVA[i]);
            }
        }
        return  Puntostotales;
    }
    //Suma total de los Puntos Obtenidos Previamente
    private static int Puntostotal(int puntosTCC, int puntosTCG, int puntosTCP, int puntosTCT, int puntosCP, int puntosCV, int puntos36m, int puntos60m) {

        int PuntosTotalesGeneral=puntos36m+puntos60m+puntosCP+puntosTCC+puntosTCP+puntosTCT+puntosCV+puntosTCG;
        return PuntosTotalesGeneral;
    }
    //Total de los Productos Financieros
    private  static  int CantidadTotal(int cantVendidaClásica, int referido, int cantVendidaGold, int referidoG, int cantVendidaPlatinum, int referidoP, int cantVendidaTop, int referidoT, int cantVendidaCréditosP, int cantVendidoCV, int referidoCV, int cantVendidoCV3, int referidoCV3, int cantVendidoCV6, int referidoCV6){
        int PFTOTAL= cantVendidaClásica+referido+cantVendidaGold+referidoG+cantVendidaPlatinum+referidoP+cantVendidaTop+referidoT+cantVendidaCréditosP+cantVendidoCV+referidoCV+cantVendidoCV3+referidoCV3+cantVendidoCV6+referidoCV6;
      return PFTOTAL;
    }
    //Total por recibir trabajador
    private  static  int ComisiónTotal(int PuntosTotales){
        int Comisionporpunto=75;
        int ComisionTotal=PuntosTotales*Comisionporpunto;

        return ComisionTotal;
    }



}