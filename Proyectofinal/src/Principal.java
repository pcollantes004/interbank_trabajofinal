public class Principal {


        //DATOS PARA COMISION DE UNA VENTA
        static String [] Productos = {"Tarjeta" ,"creditos"};
        static Double [] lineasdecredito = {1001.00 ,3001.00,5001.00,25000.00};
        static Double [] comisionTarjeta = { 0.03 , 0.035 };
        static  Double [] comisioncredito = {0.4,0.5};

        //DATOS PARA COMISION MENSUAL
        static String colaborador = "Juan";
        static double [] lineasdetcvendidas = {1550,2800,6000};
        static double comisiontc1 = 0.03 , comisiontc2 = 0.035;
        static double [] lineasdecreditosvendidass = {3500 , 4000 , 25000};
        static double comisioncred1 = 0.04 , comisioncred2 = 0.02;

        public static void main(String[] args) {
            //Para 1 venta en especifica
            double lineadecredito1 = 2300.00;
            double comision1 = 0.03;
            double comisionFinal = comisionventa(Productos,comision1,lineadecredito1);
            System.out.println("La comision final para esta venta es : " +comisionFinal);


            //Calculo comision de solo tarjeta
            double comisiontotaldeTc = Calculocomisiontotaldetc(lineasdetcvendidas,comisiontc1,comisiontc2);
            System.out.println("La comision total de Tc es : "+comisiontotaldeTc);

            // Calculo comision de solo Creditos
            double comisiontotalcred  = Calculocomisiondecreditos(lineasdecreditosvendidass,comisioncred1,comisioncred2);
            System.out.println("La comision total de creditos es : "+comisiontotalcred);

            //COMISION MENSUAL
            double comisionFinaldelmes = comisiontotaldeTc + comisiontotalcred ;
            System.out.printf("La comision a cobrar a fin de mes de %S es %.2f  ",colaborador,comisionFinaldelmes);


        }

        private static double Calculocomisiondecreditos(double[] lineasdecreditosvendidass, double comisioncred1, double comisioncred2) {
            double cantidad1 = 0;
            double cantidadfinal = 0 ;
            double cantidad2 = 0 ;
            double cantidad3 = 0;
            double lineascred1 = 0;
            double lineascred2= 0;
            double lineascred3 = 0;

            for ( int i  =0 ; i<lineasdecreditosvendidass.length ; i++){
                lineascred1 = lineasdecreditosvendidass[0];
                lineascred2 = lineasdecreditosvendidass[1];
                lineascred3 = lineasdecreditosvendidass[2];
            }
            if (lineascred1 < 25000 ){

                cantidad1 = (lineascred1*comisioncred1);

            }if (lineascred2  < 25000 ){
                cantidad2 = (lineascred2*comisioncred1);

            }if (lineascred3 >= 25000 ){
                cantidad3 = (lineascred3*comisioncred2);
            }

            cantidadfinal = cantidad1+cantidad2+cantidad3;


            return cantidadfinal;
        }

        private static double Calculocomisiontotaldetc(double[] lineasdetcvendidas, double comisiontc1, double comisiontc2) {
            double cantidad1 = 0;
            double cantidad4 = 0;
            double cantidad2 = 0;
            double cantidad3= 0;
            double cantidadfinal = 0;
            double lineastc1 = 0; double lineastc2 = 0;double lineastc3 = 0;

            for ( int i  =0 ; i<lineasdetcvendidas.length ; i++){
                lineastc1 = lineasdetcvendidas[0];
                lineastc2 = lineasdetcvendidas[1];
                lineastc3 = lineasdetcvendidas[2];

            }
            if (lineastc1 < 5000 ){

                cantidad1 = (lineastc1*comisiontc1);

            }if (lineastc2<5000){
                cantidad2 = lineastc2*comisiontc1;

            }if (lineastc3<5000){
                cantidad3 = lineastc2*comisiontc1;

            }if (lineastc3>5000){
                cantidad4 = lineastc3*comisiontc2;
            }
            cantidadfinal = cantidad1+cantidad2+cantidad4;

            return cantidadfinal ;
        }


        private static double comisionventa(String[] Productos ,double comision1,double lineadecredito1){
            double cantidad = 0;

            String productovendido = "";
            for (int i = 0 ;i< Productos.length ; i++ ){
                productovendido = Productos[i];

                if(productovendido.equalsIgnoreCase("tarjeta") && lineadecredito1>1001.00){
                    comision1 = 0.03;
                    cantidad = comision1*lineadecredito1;
                }
            }
            return cantidad;

        }
    }